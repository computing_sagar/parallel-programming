
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>


/* ---------------------------------------------------------------------
 * alloc_matrix()
 * ------------------------------------------------------------------ */


double**                  alloc_matrix

  ( int                     m,
    int                     n )

{
  double**  rows = NULL;
  double*   data = NULL;

  int       i;


  assert ( (m >= 0) && (n >= 0) );

  if ( (m == 0) || (n == 0) )
  {
    return rows;
  }

  rows = malloc ( m *     sizeof(double*) );
  data = malloc ( m * n * sizeof(double) );

  if ( ! rows || ! data )
  {
    fprintf ( stderr, "Out of memory.\n" );
    exit    ( 1 );
  }

  for ( i = 0; i < m; i++ )
  {
    rows[i] = &data[i * n];
  }

  return rows;
}


/* ---------------------------------------------------------------------
 * free_matrix()
 * ------------------------------------------------------------------ */


void                      free_matrix

  ( double**                mat )

{
  if ( mat )
  {
    free ( mat[0] );
    free ( mat );
  }
}


/* ---------------------------------------------------------------------
 * main()
 * ------------------------------------------------------------------ */
int main

  ( int                     argc,
    char**                  argv )

{

  MPI_Init(&argc,&argv) ; 

  const int  N = 1000;
  double     t;
  double     tp; 
  int        i, j, k;
  int        rank ; 
  int        numproc ; 

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);

  const int Np =  N / numproc ; 
  double    c[N];
  double    tempc[N] ; 
  double**   a = alloc_matrix (Np,N);

  printf ( "Initializing the matrix individually by each processor...\n" );
  printf ("The number of rows allocated to me is %d\n",Np );
  
  for ( j = 0; j < N; ++j ){
    for ( i = 0 ; i < Np; ++i){
      a[i][j] = (double) abs( i + (rank * Np) - j );
    }
  }
  
  printf ( "Executing the Gram-Schmidt algorithm in the parallel fashion..\n" );
  clock_t start = clock() ; 

  for ( j = 0; j < N; j++ ){
    for ( k = 0; k < j; k++ ){
      c[k] = 0.0;
      for ( i = 0; i < Np; i++ ){
        c[k] += a[i][k] * a[i][j];
      }
    }

    MPI_Allreduce(c,tempc,j,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD) ; 

    for ( k = 0; k < j; k++ ){
      for ( i = 0; i < Np; i++ ){
        a[i][j] -= tempc[k] * a[i][k];
      }
    }
    t = 0.0;

    for ( i = 0; i < Np; i++ ){
      t += a[i][j] * a[i][j];
    }

    MPI_Allreduce(&t,&tp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD) ;
    
    t = sqrt ( t ); // Finding the 2 Norm .
    
    if ( t == 0.0 )  {
      fprintf ( stderr, "Singular matrix.\n" );
      return  1;
    }

    t = 1.0 / t; // Normalization 
    
    for ( i = 0; i < Np; i++ ){
      a[i][j] *= t;
    }
  }

  clock_t end = clock() ; 

  printf("The elapsed time for the parallel Gram-Schmidt Implementation is %f seconds \n",(double) (end-start) / CLOCKS_PER_SEC );
  printf ( "Done.\n" );
  MPI_Finalize() ;
  return 0;
}

